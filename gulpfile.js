//https://www.sitepoint.com/introduction-gulp-js/
//https://medium.com/@jack.yin/exclude-directory-pattern-in-gulp-with-glob-in-gulp-src-9cc981f32116

// Sass configuration 
const gulp = require('gulp');
const sass = require('gulp-sass');
const htmlclean = require('gulp-htmlclean');
const newer = require('gulp-newer');
const minifycss = require('gulp-minify-css');
const uglify = require('gulp-uglify');
const rename = require("gulp-rename");
const clean = require('gulp-clean');
const concat = require('gulp-concat');
// const babel = require('gulp-babel');
const autoprefixer = require('gulp-autoprefixer')
// const sourcemaps = require('gulp-sourcemaps');
const devBuild = (process.env.NODE_ENV !== 'production');
const folder = {
  src: 'src/',
  build: 'build/',
  css: 'src/rs/css/',
  scss: 'src/rs/scss/',
  cssResource: 'src/rs/mainresource/css/',
  scssResource: 'src/rs/mainresource/scss/'
}
const jsInclude = [
  folder.src + 'rs/js/ctrl/**/*',
  folder.src + 'rs/js/ui/**/*',
]
const jsIncludemin = [
  folder.src + 'rs/js/ctrl/**/*.min.js',
  folder.src + 'rs/js/ui/**/*.min.js',
]

gulp.task('sass', () => {
  gulp.src(folder.scss + '*.scss') // -- add rs/scss/**/*.scss  เพื่อเอาทุก Folder
    .pipe(sass({ style: 'compressed' }))
    // .pipe(gulp.dest(folder.src + 'rs/css'))
    // .pipe(sourcemaps.init())
    // .pipe(sass().on('error', sass.logError))
    // .pipe(sourcemaps.write('./maps')) 
    // .pipe(rename({ suffix: '.min' }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('styles.min.css'))
    .pipe(minifycss())
    .pipe(gulp.dest(folder.css))
})

gulp.task('sassPages', () => {
  gulp.src(folder.scss + 'pages/**/*.scss')
    .pipe(sass({ style: 'compressed' }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(folder.css + 'pages'))
})

gulp.task('sassMainResource', () => {
  gulp.src(folder.scssResource + '*.scss')
    .pipe(sass({ style: 'compressed' }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('mainStyles.min.css'))
    .pipe(minifycss())
    // .pipe(gulp.dest(folder.cssResource))
    .pipe(gulp.dest(folder.css))
})

gulp.task('js', ['clean-scripts'], () => {
  // gulp.src([ 
  //   folder.src + 'rs/js/ctrl/**/*',
  //   folder.src + 'rs/js/ui/**/*',
  // ], { base: process.cwd() })
  gulp.src(jsInclude, { base: process.cwd() })
    .pipe(babel({
      presets: ['env']
    }))
    // .pipe(uglify())
    .pipe(rename({
      suffix: ".min",
      extname: ".js"
    }))
    // .pipe(concat('scripts.min.js'))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(uglify())
    // .pipe(gulp.dest(folder.src + 'rs/js'))
    .pipe(gulp.dest((file) => file.base));
});

gulp.task('clean-scripts', () => {
  return gulp.src(jsIncludemin, { read: false })
    .pipe(clean());
});

gulp.task('html', () => {
  var
    out = folder.build,
    page = gulp.src(folder.src + '**/*')
      .pipe(newer(out))
  if (!devBuild) {
    page = page.pipe(htmlclean())
  }

  return page.pipe(gulp.dest(out))
})

gulp.task('default', ['sass', 'sassPages', 'sassMainResource'], () => {
  gulp.watch(folder.src + '**/*.scss', ['sass'])
  gulp.watch(folder.scss + 'pages/**/*.scss', ['sassPages'])
  gulp.watch(folder.scssResource + '**/**.scss', ['sassMainResource'])
})
